FROM python:3.7-alpine

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN pip install psycopg2

RUN mkdir /app
ADD . /app

CMD ["python", "/app/scripts/new_page_monitor.py"]