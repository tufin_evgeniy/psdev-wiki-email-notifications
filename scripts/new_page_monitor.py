from time import sleep
from datetime import datetime

import sys
import os
import ssl
import logging
import smtplib
import psycopg2
import traceback

context = ssl._create_unverified_context()
logger = logging.getLogger("New_Pages_Monitor")

output_file_handler = logging.FileHandler('/var/log/pages_mailer.log')
stdout_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(output_file_handler)
logger.addHandler(stdout_handler)

logger.setLevel(logging.DEBUG)

p_db = os.environ["POSTGRES_DB"]
pg_usr = os.environ["POSTGRES_USER"]
pg_pass = os.environ["POSTGRES_PASSWORD"]

usr = os.environ["EMAIL"]
pwd = os.environ["EMAIL_PASS"]

params = {
    'dbname': p_db,
    'user': pg_usr,
    'password': pg_pass,
    'host': 'db',
    'port': 5432
}

email_template = """From: wiki-ps-dev@wsus.tufin.com
To: ps-dev-emea@tufin.com
Subject: New page created in wiki
Hi guys, new page titled '{page_title}' created by {author} is posted.
Check it out here: http://{localhost}/en/{path}
"""
datetimeformat = r"%Y-%m-%dT%H:%M:%S.%f"


def send_mail(page_title, author, path):
    server = smtplib.SMTP('192.168.1.145', 25)
    try:
        server.ehlo()
        # server.starttls()
        # server.login("", "")
        mail_body = email_template.format(page_title=page_title, author=author, path=path, localhost="192.168.204.233")
        server.sendmail("wiki-ps-dev@wsus.tufin.com", ["ps-dev-emea@tufin.com"], mail_body)
        logger.info("email has been sent")
    except Exception as e:
        logger.error(f"Error sending email for {page_title}: {e}")
    finally:
        logger.info("Closing mail server client")
        server.close()


def get_new_pages_from_db():
    try:
        conn = psycopg2.connect(**params)
    except Exception as e:
        logger.error(f"Error while trying to connect to database: {e}")
        logger.error(traceback.format_exc())

        return []
    cursor = conn.cursor()
    try:
        cursor.execute(
            "select title, p.path, p.\"createdAt\", u.email from  pages p, users u where \"authorId\"::INTEGER=u.id;")
        results = cursor.fetchall()
        return results
    finally:
        cursor.close()
        conn.close()


def main():
    last_run = datetime.now()
    logger.info("Starting pages monitoring")
    while True:
        try:
            new_pages = get_new_pages_from_db()
            logger.info(f"Total pages found: {len(new_pages)}")
            new_pages = list(filter(lambda x: datetime.strptime(x[2].strip("Z"), datetimeformat) > last_run, new_pages))
            if not new_pages:
                logger.info("No new pages found")
            else:
                logger.info(f"New pages found {len(new_pages)}")
                last_run = datetime.now()
                for page in new_pages:
                    send_mail(page[0], page[3], page[1])
        except Exception as e:
            logger.error("Uncaught error: {e}")
            logger.error(traceback.format_exc())
        sleep(120)


if __name__ == "__main__":
    main()