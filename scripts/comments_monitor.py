from time import sleep
from datetime import datetime

import sys
import os
import ssl
import logging
import smtplib
import psycopg2
import traceback

context = ssl._create_unverified_context()
logger = logging.getLogger("New_Comments_Monitor")

output_file_handler = logging.FileHandler('/var/log/comments_mailer.log')
stdout_handler = logging.StreamHandler(sys.stdout)

logger.addHandler(output_file_handler)
logger.addHandler(stdout_handler)

logger.setLevel(logging.DEBUG)


p_db = os.environ["POSTGRES_DB"]
pg_usr = os.environ["POSTGRES_USER"]
pg_pass = os.environ["POSTGRES_PASSWORD"]

usr = os.environ["EMAIL"]
pwd = os.environ["EMAIL_PASS"]

params = {
    'dbname': p_db,
    'user': pg_usr,
    'password': pg_pass,
    'host': 'db',
    'port': 5432
}

email_template = """From: wiki-ps-dev@wsus.tufin.com
To: ps-dev-emea@tufin.com
Subject: Someone posted new comment
Hi guys, looks like {author} posted new comment on the page {page_title}.
Join discussion here: http://{localhost}/en/{path}
"""

datetimeformat = r"%Y-%m-%dT%H:%M:%S.%f"


def send_mail(author, page_title, path):
    server = smtplib.SMTP('192.168.1.145', 25)
    try:
        server.ehlo()
        # server.starttls()
        # server.login("", "")
        mail_body = email_template.format(page_title=page_title, author=author, path=path, localhost='192.168.204.233')
        server.sendmail("wiki-ps-dev@wsus.tufin.com", ["ps-dev-emea@tufin.com"], mail_body)
        logger.info("email has been sent")
    except Exception as e:
        logger.error(f"Error sending email for {page_title}: {e}")
    finally:
        logger.info("Closing mail server client")
        server.close()


def get_comments():
    try:
        conn = psycopg2.connect(**params)
    except Exception as e:
        logger.error(f"Error while trying to connect to database: {e}")
        logger.error(traceback.format_exc())

        return []
    cursor = conn.cursor()
    try:
        cursor.execute(
            "select c.name, c.\"createdAt\", p.title, p.path from comments as c, pages as p where c.\"pageId\"::INTEGER=p.id;")
        results = cursor.fetchall()
        return results
    finally:
        cursor.close()
        conn.close()


def main():
    last_run = datetime.now()
    logger.info("Starting comments monitoring")
    while True:
        try:
            new_comments = get_comments()
            logger.info(f"Total pages found: {len(new_comments)}")
            new_comments = list(filter(lambda x: datetime.strptime(x[1].strip("Z"), datetimeformat) > last_run, new_comments))
            if not new_comments:
                logger.info("No new comments found")
            else:
                logger.info(f"New pages found {len(new_comments)}")
                last_run = datetime.now()
                for page in new_comments:
                    send_mail(page[0], page[2], page[3])
        except Exception as e:
            logger.error("Uncaught error: {e}")
            logger.error(traceback.format_exc())
        sleep(120)


if __name__ == "__main__":
    main()