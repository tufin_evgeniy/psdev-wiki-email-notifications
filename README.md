# psdev-wiki-email-notifications

WikiJS doesn't send email by itself. So we are running this service in container to monitor new pages and send notifications.

This package contains two scripts.
1. comments_monitor.py - Monitors and sends notifications on new comments added to pages.
2. new_page_monitor.py - Monitors and sends notifications on every new page created by users. 